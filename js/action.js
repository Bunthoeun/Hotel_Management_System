var pName=document.getElementById('pname');
var pIdcard = document.getElementById('pidcard');
var pPhone = document.getElementById('pphone');
var pAddress=document.getElementById('paddress');
var pGender=document.getElementById('optGender');

var id = 0;
var rIndex = -1;

var parText = /[a-z][A-Z]/gi;
var digit=/\d/g;
var space = /\s/g;

function namevalidation(){
    var cname=document.getElementById("custname").value;
    if(cname==""||cname==null){
        pName.innerHTML="* Customer's name cannot null";
        return false;
    }
    else if(!parText.test(cname)||digit.test(cname)){
        pName.innerHTML="*Customer's cannot be a number";
        return false;
    }
    else if(cname.match(parText)){
        pName.innerHTML="";
        return true;
    }
}

function idcardvalidation(){
    var cidcard=document.getElementById("idcard").value;
    if(cidcard==""||cidcard==null){
        pIdcard.innerHTML="*Customer's ID cannot null";
        return false;
    }
    else if(isNaN(cidcard)){
        pIdcard.innerHTML="*ID Card must be number only"
        return false;
    }
    else if (cidcard.length<9){
        pIdcard.innerHTML="* ID Card is at least 9 numbers "
        return false;
    }
    else {
        pIdcard.innerHTML="";
        return true;
    }
}
function contactvalidation(){
    var cphone=document.getElementById("pnumber").value;
    if(cphone==""||cphone==null){
        pPhone.innerHTML="*Contact cannot null";
        return false;
    }
    else if(isNaN(cphone)){
        pPhone.innerHTML="*Contact must be Character only"
        return false;
    }
    else if (cphone.length<9){
        pPhone.innerHTML="* Phone number is at least 9 numbers "
        return false;
    }
    else {
        pPhone.innerHTML="";
        return true;
    }
}
function addressvalidation(){
    var caddress= document.getElementById("address").value;
    if(caddress==""||caddress==null){
        pAddress.innerHTML="*Address cannot null";
        return false;
    }

}
function checkGender(){
    var cGender=document.getElementById("gender");
    console.log(cGender);
}
function submit(){
    checkGender();
}