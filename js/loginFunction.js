$(document).ready(function() {
    $("#btnSignin").click(function(){
        var name = checkName();
        var password = checkPass();
        if(name == false && password == false) {
            window.location.href = "index.html";
        }
    })

    $("#userName").blur(function() {
        checkName();
    });

    $("#pwd").blur(function() {
        checkPass();
    })

    function checkName() {
        var username = $("#userName").val();
        if(username == "") {
            $("#message p").text("Username cannot be empty.")
            $("#message").css({"background-color": "tomato"})

        } else if(username != "Admin") {
            $("#message p").text("Username is incorrect.")
        } else {
            return false;
        }
    }

    function checkPass() {
        var pass = $("#pwd").val();
        if(pass == "") {
            $("#message p").text("Password cannot be empty.")
            $("#message").css({"background-color": "tomato"})

        } else if(pass != "12345678") {
            $("#message p").text("Password is incorrect.")
        } else {
            return false;
        }
    }
});